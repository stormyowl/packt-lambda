#!/bin/bash

mkdir package
pip install --target ./package git+https://gitlab.com/packt-cli/packt-cli.git@master
cd package
zip -r ../lambda.zip .
cd ..
zip -g lambda.zip lambda_function.py
